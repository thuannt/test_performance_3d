﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void CameraController::Start()
extern void CameraController_Start_mBDE87C2FCF352957C2B86B67610667663422FBE6 (void);
// 0x00000002 System.Void CameraController::Update()
extern void CameraController_Update_m3C257AC762117CFDDAD03C9C4FBBFDE51C61D534 (void);
// 0x00000003 System.Void CameraController::.ctor()
extern void CameraController__ctor_m07EC5A8C82742876097619BE7DD9043F47327DAE (void);
static Il2CppMethodPointer s_methodPointers[3] = 
{
	CameraController_Start_mBDE87C2FCF352957C2B86B67610667663422FBE6,
	CameraController_Update_m3C257AC762117CFDDAD03C9C4FBBFDE51C61D534,
	CameraController__ctor_m07EC5A8C82742876097619BE7DD9043F47327DAE,
};
static const int32_t s_InvokerIndices[3] = 
{
	832,
	832,
	832,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	3,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
