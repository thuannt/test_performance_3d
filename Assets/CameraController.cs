using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public GameObject SkeletonObject;
    public Vector3 worldPosition;
    public Camera MainCamera;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            worldPosition.y = 0;
            worldPosition.x = Random.Range(-20, 20);
            worldPosition.z = Random.Range(-20, 20);
            Instantiate(SkeletonObject, worldPosition, Quaternion.identity);
        }
    }
}
